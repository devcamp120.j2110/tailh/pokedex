// Interface
export interface Pokemon {
  name: string;
  id: number;
  sprites: {
    front_default: string;
  };
}


export interface PokeDetail {
  id:number;
  open: boolean;
}