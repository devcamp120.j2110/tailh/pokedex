import axios from "axios";
import React, { useState, useEffect } from "react";
import "./App.css";
import { PokeDetail, Pokemon } from "./interface";

const App: React.FC = () => {
  // State
  const [pokemons, setPokemons] = useState<Pokemon[]>([]);
  const [apiUrl, setApiUrl] = useState<string>(
    "https://pokeapi.co/api/v2/pokemon?limit=20&offset=0"
  );
  const [loading, setLoading] = useState<boolean>(false);
  const [pokeDetail, setPokeDetail] = useState<PokeDetail>({
    id: 0,
    open: false,
  });

  // Call API
  const getPokemon = async () => {
    const res = await axios.get(apiUrl);
    let responsePokemons: any = [];
    // Loop call api get detail poke
    for (const pokemon of res.data.results) {
      setLoading(true);
      const pokeDetail = await axios.get(
        `https://pokeapi.co/api/v2/pokemon/${pokemon.name}`
      );
      responsePokemons.push(pokeDetail.data);
    }
    // Set State
    setLoading(false);
    setPokemons([...pokemons, ...responsePokemons]);
    setApiUrl(res.data.next);
  };

  // View Detail
  const viewDetailPokemon = (paramPokemon: Pokemon) => {
    setPokeDetail({
      id: paramPokemon.id,
      open: true,
    });
  };

  useEffect(() => {
    getPokemon();
  }, []);

  return (
    <div className="container">
      <h1 className="app-header">PokeDex</h1>
      {/* List */}
      <div className="list-pokemon">
        {pokemons.map((pokemon) => {
          return (
            <div
              onClick={() => {
                viewDetailPokemon(pokemon);
              }}
              className="pokemon"
              key={pokemon.id}
            >
              <h4 className="pokemon-id">#{pokemon.id}</h4>
              <div className="pokemon-img">
                <img src={pokemon.sprites.front_default} alt="pokeDetail" />
              </div>
              <h3 className="pokemon-name">{pokemon.name}</h3>
            </div>
          );
        })}
      </div>
      {/* Load More Button*/}
      <div className="btn" onClick={getPokemon}>
        {loading ? "Loading" : "Load more"}
      </div>
    </div>
  );
};

export default App;
